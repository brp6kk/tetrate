''' Contains an abstract class outlining a Piece, then several children
    classes outlining specific pieces.  Additionally contains a method to
    return a random Piece child.
'''


import pygame
import random
import sys
from pygame.locals import *
from abc import ABC, abstractmethod
import components.data as data


''' Returns the a default random child of Piece.
'''
def get_random_piece():
    # List of pieces
    pieces = ( ClevelandZ, OrangeRicky, Smashboy, RhodeIslandZ, Hero,
               BlueRicky, Teewee )
    # Generates random number from 0 to one less than amount of pieces.
    num = random.randrange(0, len(pieces))
    # Creates and returns piece in default position.
    piece = pieces[num]()
    return (piece)


''' General outline for a Tetrate piece.  Note that this has to be
    extended to create specific pieces.
'''
class Piece(ABC):
    ''' Creates new general Piece.
    '''
    def __init__(self, orientations, color, row, column):
        self.orientations = orientations
        self.color = color
        self.row = row
        self.column = column
        self.current_orientation = 0
    
    ''' Rotates piece clockwise.
    '''
    def rotate_clockwise(self):
        # Next orientation.
        self.current_orientation += 1
        
        # Goes to beginning of orientation tuple if the new position does 
        # not exist.
        if self.current_orientation >= len(self.orientations):
            self.current_orientation = 0
    
    ''' Rotates piece counterclockwise.
    '''
    def rotate_counterclockwise(self):
        #Previous orientation.
        self.current_orientation -= 1
        
        # Goes to end of orientation tuple if the new position does 
        # not exist.
        if self.current_orientation < 0:
            self.current_orientation = len(self.orientations) - 1
    
    ''' Moves piece down by one row.
    '''
    def move_down(self):
        self.row += 1
    
    ''' Moves piece horizontally.
        If direction is negative, the piece will move one block to the left.
        If direction is positive, the piece will move one block to the right.
    '''
    def move_horizontal(self, direction):
        # No movement by default.
        movement = 0
        
        # Changes movement based on direction parameter.
        # This is done to ensure that the piece moves a maximum of 1 block.
        # Negative indicates a leftward movement.
        if direction < 0:
            movement = -1
        # Positive indicates a rightward movement.
        elif direction > 0:
            movement = 1
        # If direction is 0, no movemement will occur; this was already
        # designated by giving it a default value of 0.
        
        self.column += movement
    
    ''' Returns the current orientation of the piece.
    '''
    def get_current_orientation(self):
        return (self.orientations[self.current_orientation])
    
    ''' Children will use this method to create and return all possible
        orientations of the piece.
    '''
    @abstractmethod
    def get_orientations(): pass

class ClevelandZ(Piece):
    ''' Crates a new ClevelandZ Piece.
    '''
    def __init__(self, row = data.default_row, column = data.default_column):
        super().__init__(ClevelandZ.get_orientations(), data.red, row, column)
    
    ''' Returns all possible orientations of this Piece.
    '''
    @staticmethod
    def get_orientations():
        orientations = ( ( ( data.empty, data.empty, data.empty ),
                           (  data.full,  data.full, data.empty ),
                           ( data.empty,  data.full,  data.full ) ),
                         ( ( data.empty,  data.full, data.empty ),
                           (  data.full,  data.full, data.empty ),
                           (  data.full, data.empty, data.empty ) ), 
                         ( (  data.full,  data.full, data.empty ),
                           ( data.empty,  data.full,  data.full ),
                           ( data.empty, data.empty, data.empty ) ),
                         ( ( data.empty, data.empty,  data.full ),
                           ( data.empty,  data.full,  data.full ),
                           ( data.empty,  data.full, data.empty ) ) )
        return (orientations)

class OrangeRicky(Piece):
    ''' Creates a new OrangeRicky Piece.
    '''
    def __init__(self, row = data.default_row, column = data.default_column):
        super().__init__(OrangeRicky.get_orientations(), data.orange, 
                         row, column)
    
    ''' Returns all possible orientations of this Piece.
    '''
    @staticmethod
    def get_orientations():
        orientations = ( ( ( data.empty, data.empty, data.empty ),
                           (  data.full,  data.full,  data.full ),
                           (  data.full, data.empty, data.empty ) ),
                         ( (  data.full,  data.full, data.empty ),
                           ( data.empty,  data.full, data.empty ),
                           ( data.empty,  data.full, data.empty ) ),
                         ( ( data.empty, data.empty,  data.full ),
                           (  data.full,  data.full,  data.full ),
                           ( data.empty, data.empty, data.empty ) ),
                         ( ( data.empty,  data.full, data.empty ),
                           ( data.empty,  data.full, data.empty ),
                           ( data.empty,  data.full,  data.full ) ) )
        return (orientations)

class Smashboy(Piece):
    ''' Creates a new Smashboy Piece.
    '''
    def __init__(self, row = data.default_row, column = data.default_column):
        super().__init__(Smashboy.get_orientations(), data.yellow, row, column)
    
    ''' Returns all possible orientations of this Piece.
    '''
    @staticmethod
    def get_orientations():
        orientations = ( ( ( data.empty, data.empty ),
                           (  data.full,  data.full ),
                           (  data.full,  data.full ) ), 
                         ( ( data.empty, data.empty ),
                           (  data.full,  data.full ),
                           (  data.full,  data.full ) ),
                         ( ( data.empty, data.empty ),
                           (  data.full,  data.full ),
                           (  data.full,  data.full ) ),
                         ( ( data.empty, data.empty ),
                           (  data.full,  data.full ),
                           (  data.full,  data.full ) ) )
        return (orientations)

class RhodeIslandZ(Piece):
    ''' Creates a new RhodeIslandZ Piece.
    '''
    def __init__(self, row = data.default_row, column = data.default_column):
        super().__init__(RhodeIslandZ.get_orientations(), data.green, 
                         row, column)
    
    ''' Returns all possible orientations of this Piece.
    '''
    @staticmethod
    def get_orientations():
        orientations = ( ( ( data.empty, data.empty, data.empty ),
                           ( data.empty,  data.full,  data.full ),
                           (  data.full,  data.full, data.empty ),
                           ( data.empty, data.empty, data.empty ) ),
                         ( ( data.empty, data.empty, data.empty ),
                           ( data.empty,  data.full, data.empty ),
                           ( data.empty,  data.full,  data.full ),
                           ( data.empty, data.empty,  data.full ) ),
                         ( ( data.empty, data.empty, data.empty ),
                           ( data.empty, data.empty, data.empty ),
                           ( data.empty,  data.full,  data.full ),
                           (  data.full,  data.full, data.empty ) ),
                         ( ( data.empty, data.empty, data.empty ),
                           (  data.full, data.empty, data.empty ),
                           (  data.full,  data.full, data.empty ),
                           ( data.empty,  data.full, data.empty ) ) )
        return (orientations)

class Hero(Piece):
    ''' Creates a new Hero Piece.
    '''
    def __init__(self, row = data.default_row, column = data.default_column):
        super().__init__(Hero.get_orientations(), data.cyan, row, column)
    
    ''' Returns all possible orientations of this Piece.
    '''
    @staticmethod
    def get_orientations():
        orientations = ( ( ( data.empty, data.empty, data.empty, data.empty ),
                           (  data.full,  data.full,  data.full,  data.full ),
                           ( data.empty, data.empty, data.empty, data.empty ),
                           ( data.empty, data.empty, data.empty, data.empty ) ),
                         ( ( data.empty, data.empty,  data.full, data.empty ),
                           ( data.empty, data.empty,  data.full, data.empty ) ,
                           ( data.empty, data.empty,  data.full, data.empty ),
                           ( data.empty, data.empty,  data.full, data.empty ) ),
                         ( ( data.empty, data.empty, data.empty, data.empty ),
                           ( data.empty, data.empty, data.empty, data.empty ),
                           (  data.full,  data.full,  data.full,  data.full ),
                           ( data.empty, data.empty, data.empty, data.empty ) ),
                         ( ( data.empty,  data.full, data.empty, data.empty ),
                           ( data.empty,  data.full, data.empty, data.empty ),
                           ( data.empty,  data.full, data.empty, data.empty ),
                           ( data.empty,  data.full, data.empty, data.empty ) ), )
        return (orientations)

class BlueRicky(Piece):
    ''' Creates a new BlueRicky Piece.
    '''
    def __init__(self, row = data.default_row, column = data.default_column):
        super().__init__(BlueRicky.get_orientations(), data.blue, row, column)
    
    ''' Returns all possible orientations of this Piece.
    '''
    @staticmethod
    def get_orientations():
        orientations = ( ( ( data.empty, data.empty, data.empty ),
                           (  data.full,  data.full,  data.full ),
                           ( data.empty, data.empty,  data.full ) ),
                         ( ( data.empty,  data.full, data.empty ),
                           ( data.empty,  data.full, data.empty ),
                           (  data.full,  data.full, data.empty ) ),
                         ( (  data.full, data.empty, data.empty ),
                           (  data.full,  data.full,  data.full ),
                           ( data.empty, data.empty, data.empty ) ),
                         ( ( data.empty,  data.full,  data.full ),
                           ( data.empty,  data.full, data.empty ),
                           ( data.empty,  data.full, data.empty ) ) )
        return (orientations)
    
class Teewee(Piece):
    ''' Creates a new Teewee Piece.
    '''
    def __init__(self, row = data.default_row, column = data.default_column):
        super().__init__(Teewee.get_orientations(), data.purple, row, column)
    
    ''' Returns all possible orientations of this Piece.
    '''
    @staticmethod
    def get_orientations():
        orientations = ( ( ( data.empty, data.empty, data.empty ),
                           (  data.full,  data.full,  data.full ),
                           ( data.empty,  data.full, data.empty ) ),
                         ( ( data.empty,  data.full, data.empty ),
                           (  data.full,  data.full, data.empty ),
                           ( data.empty,  data.full, data.empty ) ),
                         ( ( data.empty,  data.full, data.empty ),
                           (  data.full,  data.full,  data.full ),
                           ( data.empty, data.empty, data.empty ) ),
                         ( ( data.empty,  data.full, data.empty ),
                           ( data.empty,  data.full,  data.full ),
                           ( data.empty,  data.full, data.empty ) ) )
        return (orientations)
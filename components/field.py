''' Contains a class representing a game field.
'''


import pygame
from pygame.locals import *
import components.data as data
import components.pieces


''' Outline for a game field, which contains many pieces.
'''
class Field():
    ''' Creates a new Field.
    '''
    def __init__(self, num_rows, num_columns):
        # These variables are created from parameters.
        self.num_rows = num_rows
        self.num_columns = num_columns
        
        # Actual field is a grid, which by default is empty.
        self.matrix = []
        # Creates a row.
        for row in range(self.num_rows):
            row_data = []
            # Populates the row with the appropriate amount of spaces.
            for column in range(num_columns):
                row_data.append(data.empty)
            # Adds row to grid.
            self.matrix.append(row_data)
    
    ''' Places each colored block of a piece in the appropriate place
        of the field, assuming all colored blocks are actually in 
        the field.
    '''
    def place_piece(self, piece):
        # Gets the orientation to draw and the color to use.
        orientation = piece.orientations[piece.current_orientation]
        color = piece.color
        
        # For each non-empty cell in the orientation, add the color.
        for row in range(len(orientation)):
            for column in range(len(orientation[row])):
                try:
                    if (orientation[row][column] != data.empty):
                        self.matrix[piece.row + row][piece.column + column] = color
                except IndexError:
                    return
    
    ''' Clears a row from the grid and moves all rows above it down to
        occupy the now-empty space.
    '''
    def delete_row(self, row):
        # Starting at the row to delete, copies the above row into the row.
        for num in range(row, 0, -1):
            self.matrix[num] = self.matrix[num - 1][:]
        
        # Highest row is filled with empty slots.
        for column in range(self.num_columns):
            self.matrix[0][column] = data.empty
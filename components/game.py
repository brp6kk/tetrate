''' Contains a class outlining a single game of Tetrate.
'''


import pygame
from pygame.locals import *
import components.data as data
import components.exceptions as exceptions
import components.field as field
import components.graphics as graphics
import components.io as io
import components.pieces as pieces
import components.stats as stats


''' Outlines the necessary behaviors for a game of Tetrate.
'''
class Game():
    ''' Creates a new game of Tetrate.
    '''
    def __init__(self, info):
        self.info = info
    
    ''' Plays through a full game of Tetrate.  Events are checked, and the
        program reacts accordingly.
    '''
    def play(self):
        # Loops until the game is over; this happens when a new piece
        # is unable to enter the top of the playing field.
        while (True):
            # Checks all events.
            for event in pygame.event.get():
                # Quit event - terminate the program.
                if event.type == QUIT:
                    io.terminate(self.info)
                # Keyup event.
                elif event.type == KEYUP:
                    # Escape key terminates the program.
                    if event.key == K_ESCAPE:
                        io.terminate(self.info)
                    # User is no longer holding down left/a key.
                    elif (event.key == K_LEFT or event.key == K_a):
                        self.info.left = False
                    # User is no longer holding down right/d key.
                    elif (event.key == K_RIGHT or event.key == K_d):
                        self.info.right = False
                    # User is no longer holding down down/s key.
                    elif (event.key == K_DOWN or event.key == K_s):
                        self.info.soft_drop = False
                    # Pauses game on lshift/rshift keyup.
                    elif (event.key == K_LSHIFT or event.key == K_RSHIFT):
                        graphics.pause(self.info)
                # Keydown event.
                elif event.type == KEYDOWN:
                    # Escape key terminates the program.
                    if event.key == K_ESCAPE:
                        io.terminate(self.info)
                    # User begins holding down left/a key.
                    elif (event.key == K_LEFT or event.key == K_a):
                        self.info.left = True
                    # User begins holding down right/d key.
                    elif (event.key == K_RIGHT or event.key == K_d):
                        self.info.right = True
                    # User begins holding down down/s key.
                    elif (event.key == K_DOWN or event.key == K_s):
                        self.info.soft_drop = True
                    # Causes a single counterclockwise turn, if such a turn
                    # is legal.
                    elif (event.key == K_COMMA or event.key == K_z):
                        try:
                            self.rotate_counterclockwise()
                        except exceptions.CannotMovePieceException: pass
                    # Causes a single clockwise turn, if such a turn is legal.
                    elif (event.key == K_PERIOD or event.key == K_x):
                        try:
                            self.rotate_clockwise()
                        except exceptions.CannotMovePieceException: pass
            
            # Checks horizontal directions.
            if (self.info.left): # User is holding down left key.
                # A left move may not succeed; if it doesn't, do nothing.
                try:
                    self.horizontal_move(-1)
                except exceptions.CannotMovePieceException: pass
            elif (self.info.right): # User is holding down right key.
                # A right move may not succeed; if it doesn't, do nothing.
                try:
                    self.horizontal_move(1)
                except exceptions.CannotMovePieceException: pass
            
            # If the user is initiating a soft drop or if the time since
            # the last natural drop exceeds the level's speed...
            if ((self.info.soft_drop) 
                or (self.info.since_last_natural_drop >= self.info.speed)):
                # Tries to move the current piece down by one block.
                try:
                    self.downward_move()
                # Error: cannot move piece down anymore.
                except exceptions.CannotMovePieceException:
                    # Tries to place the current piece.
                    try:
                        self.place_current_piece()
                    # Error: Piece still cannot be placed; the game is over.
                    # Show the field with this illegal piece on it so that
                    # user is aware of reason for game over.
                    except exceptions.CannotMovePieceException:
                        graphics.game_screen(self.info)
                        pygame.display.update()
                        raise exceptions.GameOverException
                    # Success: Reset movement variables and check to see
                    # if a full row has been achieved.
                    else:
                        self.info.left = False
                        self.info.right = False
                        self.info.soft_drop = False
                        self.check_for_full_rows()
                # Success: Adds to score if the piece is being softdropped
                # OR reset time since last natural drop, accordingly.
                else:
                    if (self.info.soft_drop):
                        self.info.score += data.default_soft_drop_score
                    else:
                        self.info.since_last_natural_drop = 0
            
            # Updates graphics.
            graphics.game_screen(self.info)
            pygame.display.update()
            
            # Updates time since last natural drop:
            # Adds time that passed during last clock tick,
            # Adds time that passed since last clock tick.
            self.info.since_last_natural_drop += (
                    self.info.fpsclock.get_rawtime()
                    + (1000 / data.user_fps))
            
            # Delays game appropriately.
            self.info.fpsclock.tick(data.user_fps)
    
    ''' Attempts to move the current piece horizontally.  Direction parameter
        should be negative if a left move is desired, or positive if a 
        right move is desired.
    '''
    def horizontal_move(self, direction):
        # Moves piece.
        self.info.current_piece.move_horizontal(direction)
        
        # If this move is not legal, move piece back and throw exception.
        if not self.is_legal_position(self.info.current_piece):
            self.info.current_piece.move_horizontal(-direction)
            raise exceptions.CannotMovePieceException
    
    ''' Attempts to move the current piece down by one block.
    '''
    def downward_move(self):
        # Moves piece.
        self.info.current_piece.move_down()
        
        # If this move is not legal, move piece back and throw exception.
        if not self.is_legal_position(self.info.current_piece):
            self.info.current_piece.row -= 1
            raise exceptions.CannotMovePieceException
    
    ''' Attempts to rotate the current piece clockwise.
    '''
    def rotate_clockwise(self):
        # Rotates piece.
        self.info.current_piece.rotate_clockwise()
        
        # If this move is not legal, rotate piece back and throw exception.
        if not self.is_legal_position(self.info.current_piece):
            self.info.current_piece.rotate_counterclockwise()
            raise exceptions.CannotMovePieceException
    
    ''' Attempts to rotate the current piece counterclockwise.
    '''
    def rotate_counterclockwise(self):
        # Rotates piece.
        self.info.current_piece.rotate_counterclockwise()
        
        # If this move is not legal, rotate piece back and throw exception.
        if not self.is_legal_position(self.info.current_piece):
            self.info.current_piece.rotate_clockwise()
            raise exceptions.CannotMovePieceException
    
    ''' Determines if a piece can legally be placed on the playing field.
    '''
    def is_legal_position(self, piece):
        # Gets the current orientation of the piece.
        orientation = self.info.current_piece.get_current_orientation()
        
        # Loops through all blocks of the orientation.
        for row in range(len(orientation)):
            for column in range(len(orientation[row])):
                # Calculates at which block of the field that the piece
                # currently is located.
                block_row = row + self.info.current_piece.row
                block_column = column + self.info.current_piece.column
                
                # If the orientation actually is full at this block.
                if (orientation[row][column] == data.full):
                    # If the block is not on the playing field, the move
                    # is not legal.
                    if (block_row < 0 
                        or block_row >= self.info.playing_field.num_rows
                        or block_column < 0
                        or block_column >= self.info.playing_field.num_columns):
                        return (False)
                    # If the block is on an already-occupied block of 
                    # the playing field, the move is not legal.
                    elif not (self.info.playing_field.matrix[block_row][block_column] == data.empty):
                        return (False)
        
        # If this method hasn't return False by this point, the move is legal.
        return (True)
    
    ''' Places the current piece on the playing field.
    '''
    def place_current_piece(self):
        # Places piece and move the next piece to the "current piece" position.
        self.info.playing_field.place_piece(self.info.current_piece)
        self.info.current_piece = self.info.next_piece
        
        # If the newly-current piece cannot be placed on the playing field,
        # the game is over.
        if not self.is_legal_position(self.info.current_piece):
            raise exceptions.CannotMovePieceException
        
        # Randomly chooses a new piece to be the next-to-fall piece.
        self.info.next_piece = pieces.get_random_piece()
    
    ''' Checks to see if there are any full rows on the playing field.
        If so, these lines are cleared.
    '''
    def check_for_full_rows(self):
        # Gets playing field.
        matrix = self.info.playing_field.matrix
        # Counter variable.
        lines_cleared = 0
        
        # Loops through entire playing field.
        for row in range(len(matrix)):
            # Full by default.
            full_row = True
            # Loops through all cells of row.
            for column in range(len(matrix[row])):
                # If there are any empty cells in the row, this is not
                # a full row.
                if (matrix[row][column] == data.empty):
                    full_row = False
            # If this is a full row, delete it and increment counter by 1.
            if (full_row):
                self.info.playing_field.delete_row(row)
                lines_cleared += 1
        
        # If any lines were cleared, update score.
        if (lines_cleared > 0):
            self.info.add_to_score(lines_cleared)
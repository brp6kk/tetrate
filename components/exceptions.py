''' Contains multiple unique exceptions that may be necessary in 
    a Tetrate game.
'''


''' Meant to be thrown when a piece cannot occupy a particular space on the 
    field without overlapping with another.
'''
class CannotMovePieceException(Exception): pass

''' Meant to be thrown when new pieces cannot enter the playing field due to 
    tetrominos reaching the top.  Such an occurrence indicates that the game 
    is over.
'''
class GameOverException(Exception): pass

''' Meant to be thrown if the data within the save file is improperly 
    formatted.  Data should be in the format "Name,Score", with Score 
    being an integer, and all scores being on separate lines.  A list of
    scores should contain a tuple formatted as (Name, Score).
'''
class ImproperScoreFormatException(Exception): pass
''' Contains constants relevant to Tetrate.
'''


import random


title = "Tetrate"

# Color constants:
#                R    G    B
red =          (204,   0,   0)
orange =       (204, 102,   0)
yellow =       (204, 204,   0)
green =        (  0, 204,   0)
cyan =         (  0, 204, 204)
blue =         (  0,   0, 204)
purple =       (102,   0, 204)
gray =         (206, 213, 217)
light_gray =   (241, 243, 244)
dark_gray =    (169, 182, 188)
darkest_gray = ( 33,  40,  43)
accent =       (190, 220, 233)
black =        (  0,   0,   0)
white =        (255, 255, 255)
bg_colors =  ( (245, 214, 214), (245, 222, 214), (245, 230, 214),
               (245, 237, 214), (245, 245, 214), (237, 245, 214),
               (230, 245, 214), (222, 245, 214), (214, 245, 214),
               (214, 245, 222), (214, 245, 230), (214, 245, 237),
               (214, 245, 245), (214, 237, 245), (214, 230, 245),
               (214, 222, 245), (214, 214, 245), (222, 214, 245),
               (230, 214, 245), (237, 214, 245), (245, 214, 245),
               (245, 214, 237), (245, 214, 230), (245, 214, 222),
               (245, 214, 214) )

# Save data constants:
file_name = "components/save_data.txt"
delim = ","
name_location = 0
score_location = 1
num_top_scores = 5
max_name_length = 7

# Basic screen constants:
block_size = 20
screen_width = block_size * 20
screen_height = block_size * 18

# Constants for drawing the field:
field_rows = 19
field_columns = 10
field_x_pos = 40
field_y_pos = 0
field_x_size = field_columns * block_size
field_y_size = (field_rows - 1) * block_size

# Constants for drawing the box that displays the next block to fall.
next_box_rows = 4
next_box_columns = 4
next_box_x_pos = 300
next_box_y_pos = 60
next_box_x_size = next_box_columns * block_size
next_box_y_size = next_box_rows * block_size

# Constants for drawing score, level, and lines cleared.
info_x_pos = next_box_x_pos - block_size
info_y_pos = next_box_y_pos + next_box_y_size + 2* block_size
info_x_size = 6 * block_size
info_y_size = 2 * block_size

# Scoring constants.
default_clear_scores = { 1: 40, 2: 100, 3: 300, 4: 1200}
default_soft_drop_score = 1

# Piece constants.
empty = "O"
full = "X"
default_row = 0
default_column = 3

# Game speed constants.
speed_change_factor = -20
user_fps = 15

''' Randomly chooses and returns a background color.
'''
def random_bg_color():
    index = random.randrange(0, len(bg_colors))
    color = bg_colors[index]
    return (color)
''' Contains a class containing dynamic variables, which are
    unique to each Tetrate game, and methods to update these variables.
'''


import random
import pygame
from pygame.locals import *
import components.pieces as pieces
from components.field import Field
import components.data as data
import components.io as io


''' Contains information and methods to update information relevant
    to a Tetrate game.
'''
class Stats():
    ''' Creates new collection of Stats information.
    '''
    def __init__(self, screen, fpsclock, title_font, basic_font, small_font):
        # Instance variables that start the same for every game.
        self.current_piece = pieces.get_random_piece()
        self.next_piece = pieces.get_random_piece()
        self.level = 1
        self.score = 0
        self.lines = 0
        self.speed = 1000
        self.left = False
        self.right = False
        self.soft_drop = False
        self.since_last_natural_drop = 0
        self.playing_field = Field(data.field_rows, data.field_columns)
        self.bg_color = data.random_bg_color()
        
        # Instance variables from parameters.
        self.screen = screen
        self.fpsclock = fpsclock
        self.basic_font = basic_font
        self.title_font = title_font
        self.small_font = small_font
        
        # Try getting the top scores; if this is not possible, start out with
        # a blank list of scores.
        try:
            self.top_scores = io.get_top_scores(io.get_file_data())
        except FileNotFoundError:
            self.top_scores = []
    
    ''' Updates score, number of lines cleared, and level appropriately 
        based on the number of lines cleared.
    '''
    def add_to_score(self, lines_cleared):
        # Tries updating the score and lines cleared appropriately; if an
        # error occurs, then don't bother any updating.
        try:
            self.score += (data.default_clear_scores[int(lines_cleared)] 
                           * self.level)
            self.lines += int(lines_cleared)
            self.check_level()
        except ValueError: pass
    
    ''' Increases speed based on the speed factor.
    '''
    def increase_speed(self):
        self.speed += data.speed_change_factor
    
    ''' Checks to see if the level needs to be updated.
    '''
    def check_level(self):
        # What should the level be based on the number of lines?
        new_level = int(self.lines / 10) + 1
        
        # If the expected level is greater than the real level, then it
        # and the speed both need to be updated appropriately.
        if (self.level < new_level):
            for num in range(self.level, new_level):
                self.level += 1
                self.increase_speed()
            self.new_bg_color()
    
    ''' Adds a new high score to the list of high scores, then updates
        the list to get rid of the lowest score.
    '''
    def add_to_score_list(self, name, score):
        self.top_scores.append([name, score])
        self.top_scores = io.get_top_scores(self.top_scores)
    
    ''' Randomly changes the background color to a new color.
    '''
    def new_bg_color(self):
        self.bg_color = data.random_bg_color()
    
    ''' Resets game variables, this way a Stats object can be reused
        among games.
    '''
    def new_stats(self):
        # Instance variables that start the same for every game.
        self.current_piece = pieces.get_random_piece()
        self.next_piece = pieces.get_random_piece()
        self.level = 1
        self.score = 0
        self.lines = 0
        self.speed = 900
        self.left = False
        self.right = False
        self.soft_drop = False
        self.playing_field = Field(data.field_rows, data.field_columns)
        self.bg_color = data.random_bg_color()
    
    ''' Checks to see if the passed parameter is a new high score.
    '''
    def is_high_score(self, new_score):
        # Less than the necessary number of top scores are saved -
        # No matter what, new_score should be saved.
        if (len(self.top_scores) < data.num_top_scores):
            return (True)
        
        # Loops through all saved scores; if new_score is greater than
        # any of them, there is a new high score.
        for score in self.top_scores:
            if (new_score > score[data.score_location]):
                return (True)
        
        # If the method hasn't returned True by now, then new_score is
        # not a new high score.
        return (False)
''' Contains methods to handle external data.
    Contains a class outlining an input box, which is meant to be used
    with a GUI.
'''


import sys
import pygame
from pygame.locals import *
import components.data as data
import components.exceptions as exceptions


''' Reads in and returns the data from the file.
'''
def get_file_data(file_name = data.file_name):
    # Opens the file, assuming it exists.
    try:
        rfile = open(file_name, "r")
    except FileNotFoundError:
        raise FileNotFoundError
    
    # Gets all data from file and closes file.
    file_data = rfile.readlines()
    rfile.close()
    
    # Splits one line of data on the delimiter.  The score is converted to 
    # an integer value.  This data is then added to a large list of names
    # and corresponding scores.
    split_data = []
    for index in range(len(file_data)):
        line = file_data[index].split(data.delim)
        line[data.score_location] = int(line[data.score_location])
        split_data.append(line)
        
    return(split_data)

''' Returns a String containing the data formatted to resemble a leaderboard.
'''
def format_data(file_data):
    # Gets the appropriate number of top scores.
    top_scores = get_top_scores(file_data)
    
    # Formats the scores. Each line is as follows:
    # Place_Number - Name - Score
    formatted = []
    for score_num in range(data.num_top_scores):
        line = str(score_num + 1) + " - "
        line += top_scores[score_num][data.name_location] + " - "
        line += str(top_scores[score_num][data.score_location])
        formatted.append(line)
    
    return (formatted)

''' Saves the top few scores in the data file, overwriting all old scores.
'''
def save_file_data(file_data):
    # Gets the appropriate number of top scores.
    top_scores = get_top_scores(file_data)
    
    # Opens the file, writes data using the proper format, and closes file.
    wfile = open(data.file_name, "w")
    for line in top_scores:
        wfile.write(line[data.name_location] + "," + 
                    str(line[data.score_location]) + "\n")
    wfile.close()

''' Obtains the appropriate number of top scores from the passed
    file data.  If the file data is in an improper format, then
    empty scores are returned.
'''
def get_top_scores(file_data):
    # Copies file data.
    top = file_data[:]
    
    # Tries to sort file data; if this isn't possible, discard
    # all former data because it's in the improper format.
    try:
        sort_scores_descending(top)
    except exceptions.ImproperScoreFormatException:
        top = []
    
    # If there are less than the necessary number of top scores, then
    # add empty scores.
    if (len(top) < data.num_top_scores):
        for num in range(len(top), data.num_top_scores):
            top.append(("", 0))
    # If there are more than the necessary number of top scores, then
    # cut off the scores after the necessary number.  Since the scores
    # are sorted in descending order, this should leave only the top.
    elif (len(top) > data.num_top_scores):
        top = top[:data.num_top_scores]
    
    return (top)

''' Sorts a list of score tuples in descending order.
    Proper format of scores list:
    [(Name1, Score1), (Name2, Score2), ... (NameN, ScoreN)]
    Where Score is an integer.
'''
def sort_scores_descending(scores):
    # Watches for any improperly formatted data.
    try:
        keep_looping = True # Loop control variable.
        
        # Loops until the sort is complete.
        while (keep_looping):
            # This variable will be set to true if a swap is made,
            # since at least one more check needs to occur
            # to ensure that the data is in descending order.
            keep_looping = False
            
            # Compares each score in the list to the next one.
            # Stops before the last score to prevent errors.
            for num in range(len(scores) - 1):
                # If this score is less than the next score, swap them.
                # Tuples are immutable so swapping like this is legal.
                if (scores[num][data.score_location] 
                        < scores[num + 1][data.score_location]):
                    temp = scores[num]
                    scores[num] = scores[num + 1]
                    scores[num + 1] = temp
                    keep_looping = True # Loop needs to run again.
    # Calling code needs to be what handles this mistake.
    except (IndexError, TypeError):
        raise exceptions.ImproperScoreFormatException

''' Saves score data and stops music, then exits the program.
'''
def terminate(info):
    save_file_data(info.top_scores)
    pygame.mixer.music.stop()
    pygame.quit()
    sys.exit()

''' Checks to see if a key has been pressed, returning the key if so.
'''
def key_pressed(info):
    if ( len(pygame.event.get(QUIT)) > 0 ):
        terminate(info)
    
    keyUpEvents = pygame.event.get(KEYUP)
    if len(keyUpEvents) == 0:
        return None
    if keyUpEvents[0].key == K_ESCAPE:
        terminate(info)
    return keyUpEvents[0].key

''' Blueprint for an inputbox, which is meant to be used in a Pygame
    screen to display user input.
'''
class InputBox():
    ''' Creates new input box.
    '''
    def __init__(self, width, height, x_pos, y_pos):
        self.width = width
        self.height = height
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.active = True
        self.text = ""
        self.color = data.gray
    
    ''' Adds one character to the text value of the input box.
    '''
    def add_text_char(self, letter):
        # Adds the character, assuming that doing so will not push the
        # text length over the maximum.
        if (len(self.text) < data.max_name_length):
            self.text += letter
    
    ''' Deletes a single character from the text value of the input box.
    '''
    def delete_text_char(self):
        self.text = self.text[0:-1]
    
    ''' Draws the input box on a pygame screen.
    '''
    def draw(self, info):
        # Draws the background for the text.
        pygame.draw.rect(info.screen, data.light_gray,
                         (self.x_pos, self.y_pos, self.width, self.height))
        
        # Draws the actual text.
        text_surface = info.basic_font.render(self.text, True, data.black)
        text_rect = text_surface.get_rect()
        text_rect.topleft = (self.x_pos, self.y_pos)
        info.screen.blit(text_surface, text_rect)
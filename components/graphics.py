''' Contains multiple methods used to draw GUI Pygame graphics.
'''


import pygame
from pygame.locals import *
import components.data as data
import components.stats as stats
import components.io as io


''' Draws and updates the start screen.  This screen includes the title
    of the game and a top-score leaderboard.  After this screen is drawn,
    it will remain until any key is pressed.
'''
def start_screen(info):
    # Sets up title, which will be near the top of the screen.
    title_surface = info.title_font.render(data.title, True, data.black)
    title_rect = title_surface.get_rect()
    title_rect.center = (data.screen_width / 2, 50)
    
    # Sets up leaderboard, which consists of multiple lines.
    # This will be in the middle of the screen.
    lines = io.format_data(info.top_scores)
    score_surfaces = []
    score_rects = []
    for index in range(len(lines)):
        score_surfaces.append(info.basic_font.render(lines[index], 
                              True, data.black) )
        score_rects.append(score_surfaces[index].get_rect())
        score_rects[index].center = (data.screen_width / 2, 170 + (30 * index))
    
    # Sets up instructions, which will be near the bottom of the screen.
    cont_surface = info.small_font.render("Press any key to continue",
                                          True, data.black)
    cont_rect = cont_surface.get_rect()
    cont_rect.center = (data.screen_width / 2, data.screen_height - 30)
    
    counter = 0
    
    # Loops until a key is pressed.
    while (True):
        # If a key is pressed, leave this method.
        if (io.key_pressed(info)):
            pygame.event.get() # Clear queued events.
            return
        
        # Adds background color and text.
        # Background color cycles through a list of colors.
        info.screen.fill(data.bg_colors[counter % len(data.bg_colors)])
        info.screen.blit(title_surface, title_rect)
        for index in range(len(lines)):
            info.screen.blit(score_surfaces[index], score_rects[index])
        info.screen.blit(cont_surface, cont_rect)
        
        # Updates display and counter, then waits.
        pygame.display.update()
        counter += 1
        info.fpsclock.tick(10)

''' Draws and updates the controls screen.  This screen includes a 
    description of the game's controls.  After this screen is drawn, it will
    remain until a key is pressed.
'''
def controls_screen(info):
    # Controls information.
    controls = ( "A, L-Arrow : Move left",
                 "D, R-Arrow : Move right",
                 "S, Down-Arrow : Soft drop",
                 "<, Z : Rotate counterclockwise",
                 ">, X : Rotate clockwise",
                 "L-Shift, R-Shift : Pause" )
    
    # Sets up controls text.
    # Each control will be on a different line.
    ctr_surfaces = []
    ctr_rects = []
    for index in range(len(controls)):
        ctr_surfaces.append(info.small_font.render(controls[index],
                                                   True, data.black))
        ctr_rects.append(ctr_surfaces[index].get_rect())
        ctr_rects[index].center = (data.screen_width / 2, 50 + (40 * index))
    
    # Sets up instructions, which will be near the bottom of the screen.
    cont_surface = info.small_font.render("Press any key to start",
                                          True, data.black)
    cont_rect = cont_surface.get_rect()
    cont_rect.center = (data.screen_width / 2, data.screen_height - 30)
    
    counter = 0
    
    # Loops until a key is pressed.
    while (True):
        # If a key is pressed, leave this method.
        if (io.key_pressed(info)):
            pygame.event.get() # Clear queued events.
            return
        
        # Adds background color and text.
        # Background color cycles through a list of colors.
        info.screen.fill(data.bg_colors[counter % len(data.bg_colors)])
        for index in range(len(controls)):
            info.screen.blit(ctr_surfaces[index], ctr_rects[index])
        info.screen.blit(cont_surface, cont_rect)
        
        # Updates display and counter, then waits.
        pygame.display.update()
        counter += 1
        info.fpsclock.tick(10)

''' Draws the full game screen.
'''
def game_screen(info):
    draw_frame(info)
    draw_field(info)

''' Draws the save screen.
'''
def save_screen(info):
    box = io.InputBox(140, 32, data.screen_width / 2 - 70, 250)
    save_text = ["Congratulations on",
                 "your high score!",
                 str(info.score), 
                 "Enter your name to save:"]
    
    # Keep the same background color as the one last used in the Tetrate game.
    info.screen.fill(info.bg_color)
    
    # Draws all parts of save_text.
    for index in range(len(save_text)):
        text_surface = info.small_font.render(save_text[index], 
                                              True, data.black)
        text_rect = text_surface.get_rect()
        text_rect.center = (data.screen_width / 2, 60 + index * 50)
        info.screen.blit(text_surface, text_rect)
    
    # Clears queued events.
    pygame.event.get()
    
    # Loops until the enter key is pressed.
    while (True):
        # Loops through all events.
        for event in pygame.event.get():
            # Quit event - terminate the program.
            if event.type == QUIT:
                io.terminate(info)
            # Keyup event.
            elif event.type == KEYUP:
                # Escape key terminates the program.
                if event.key == K_ESCAPE:
                    io.terminate(info)
                # Return key saves the score (if a name was entered)
                # and leaves the save screen.
                elif event.key == K_RETURN:
                    if (len(box.text) > 0):
                        info.add_to_score_list(box.text, info.score)
                    return
                # Backspace key removes one letter from the text value
                # of the input box.
                elif event.key == K_BACKSPACE:
                    box.delete_text_char()
                # Other keys should add to the text value of the input box.
                else:
                    try:
                        letter = chr(event.key).upper()
                    except TypeError: pass
                    else:
                        box.add_text_char(letter)
        
        # Redraw the box and update the screen.
        box.draw(info)
        pygame.display.update()

''' Draws the frame surrounding the playing field.  This section of the main
    game screen includes information on the next piece to fall, the current
    score, the current level, the current nuber of lines cleared, and
    some decorations.
'''
def draw_frame(info):
    info.screen.fill(data.gray)
    
    # Border of field.
    pygame.draw.rect(info.screen, data.dark_gray, 
                     (data.field_x_pos - data.block_size, data.field_y_pos, 
                      data.field_x_size + (2 * data.block_size),
                      data.field_y_size))
    
    # Next-to-fall box:
    # Border.
    pygame.draw.rect(info.screen, info.bg_color, 
                     (data.next_box_x_pos - data.block_size,
                      data.next_box_y_pos - data.block_size, 
                      data.next_box_x_size + (2 * data.block_size),
                      data.next_box_y_size + (2 * data.block_size)))
    # Box.
    pygame.draw.rect(info.screen, data.light_gray, 
                     (data.next_box_x_pos, data.next_box_y_pos, 
                      data.next_box_x_size, data.next_box_y_size))
    # Piece.
    orientation = info.next_piece.get_current_orientation()
    # Loops through all blocks of orientation.  If the block is full, draw it.
    for row in range(len(orientation)):
        for column in range(len(orientation[row])):
            if (orientation[row][column] == data.full):
                # Block border.
                pygame.draw.rect(info.screen, data.darkest_gray,
                                 (data.next_box_x_pos 
                                  + (column * data.block_size),
                                  data.next_box_y_pos
                                  + (row * data.block_size), 
                                  data.block_size, data.block_size))
                # Block color.
                pygame.draw.rect(info.screen, info.next_piece.color,
                                 (data.next_box_x_pos + 1
                                  + (column * data.block_size),
                                  data.next_box_y_pos + 1
                                  + (row * data.block_size),
                                  data.block_size - 2, data.block_size - 2))
    
    # Score/level/lines cleared information.
    information = [["SCORE", str(info.score)],
                   ["LEVEL", str(info.level)],
                   ["LINES", str(info.lines)]]
    
    for index in range(len(information)):
        # Block.
        pygame.draw.rect(info.screen, data.light_gray, 
                         (data.info_x_pos, 
                          data.info_y_pos + (index * 3 * data.block_size), 
                          data.info_x_size, data.info_y_size))
        # Left-hand accent.
        pygame.draw.rect(info.screen, data.accent, 
                         (data.info_x_pos - data.block_size, 
                          data.info_y_pos + + (index * 3 * data.block_size), 
                          data.block_size, data.info_y_size))
        # Label text.
        score_txt_surface = info.small_font.render(information[index][0], 
                                                   True, data.black)
        score_txt_rect = score_txt_surface.get_rect()
        score_txt_rect.topleft = (data.info_x_pos, 
                                  data.info_y_pos 
                                  + (index * 3 * data.block_size))
        info.screen.blit(score_txt_surface, score_txt_rect)
        # Actual number.
        score_surface = info.small_font.render(information[index][1], 
                                               True, data.black)
        score_rect = score_surface.get_rect()
        score_rect.topright = (data.screen_width, 
                               data.info_y_pos 
                               + ((index * 3 + 1) * data.block_size))
        info.screen.blit(score_surface, score_rect)
    
''' Draws the playing field.  This section of the main game screen is where
    pieces drop and remain.
'''
def draw_field(info):
    # Draws background of field.
    pygame.draw.rect(info.screen, info.bg_color, (data.field_x_pos, 
                     data.field_y_pos, data.field_x_size, data.field_y_size ))
    
    # Draws all pieces of the playing field.
    playing_field = info.playing_field
    # Goes through all blocks; any that aren't empty are drawn.
    for row in range(1, playing_field.num_rows):
        for column in range(playing_field.num_columns):
            if not (playing_field.matrix[row][column] == data.empty):
                # Block border.
                pygame.draw.rect(info.screen, data.darkest_gray,
                                 (data.field_x_pos
                                  + (column * data.block_size),
                                  data.field_y_pos
                                  + ((row - 1) * data.block_size),
                                  data.block_size, data.block_size))
                # Block color.
                pygame.draw.rect(info.screen,
                                 playing_field.matrix[row][column],
                                 (data.field_x_pos + 1
                                  + (column * data.block_size),
                                  data.field_y_pos + 1
                                  + ((row - 1) * data.block_size),
                                  data.block_size - 2, data.block_size - 2))
    
    # Draws current piece.
    orientation = info.current_piece.get_current_orientation()
    # While most drawing will include the entire orientation, if the piece
    # is at the very top of the screen, the first row needs to be skipped.
    row_start = 0
    if (info.current_piece.row == 0):
        row_start = 1
    # Loops through all blocks and draws any full blocks of the orientation.
    for row in range(row_start, len(orientation)):
        for column in range(len(orientation[row])):
            if orientation[row][column] == data.full:
                # Block border.
                pygame.draw.rect(info.screen, data.darkest_gray,
                                 (data.field_x_pos 
                                  + ((info.current_piece.column 
                                      + column) * data.block_size),
                                  data.field_y_pos 
                                  + ((info.current_piece.row 
                                      + row - 1) * data.block_size),
                                  data.block_size, data.block_size))
                # Block color.
                pygame.draw.rect(info.screen, info.current_piece.color,
                                 (data.field_x_pos + 1
                                  + ((info.current_piece.column
                                      + column) * data.block_size),
                                  data.field_y_pos + 1
                                  + ((info.current_piece.row
                                      + row - 1) * data.block_size),
                                  data.block_size - 2, data.block_size - 2))

''' Draws and updates the pause screen.  This screen covers the field
    and next-to-fall box.  After this screen is drawn, it will remain
    until a key is pressed.
'''
def pause(info):
    # Start with the frame surrounding the field.
    draw_frame(info)
    
    # Covers field.
    pygame.draw.rect(info.screen, info.bg_color, 
                     (data.field_x_pos, data.field_y_pos,
                      data.field_x_size, data.field_y_size))
    
    # Covers next-to-fall box.
    pygame.draw.rect(info.screen, data.light_gray,
                     (data.next_box_x_pos, data.next_box_y_pos,
                      data.next_box_x_size, data.next_box_y_size))
    
    # Calculates location/size of "pause" box/text.
    pause_x_loc = data.field_x_pos + (2 * data.block_size)
    pause_y_loc = data.field_y_pos + (7 * data.block_size)
    pause_x_size = 6 * data.block_size
    pause_y_size = data.block_size
    
    # Box for "PAUSE" text.
    pygame.draw.rect(info.screen, data.light_gray,
                     (pause_x_loc, pause_y_loc, pause_x_size, pause_y_size))
    
    # Actual "PAUSE" text.
    pause_surface = info.small_font.render("PAUSE", True, data.black)
    pause_rect = pause_surface.get_rect()
    pause_rect.center = (pause_x_loc + (pause_x_size / 2), 
                         pause_y_loc + (pause_y_size / 2))
    info.screen.blit(pause_surface, pause_rect)
    
    pygame.display.update()
    
    # Stays on pause screen until either l-shift or r-shift is pressed.
    while (True):
        # Loops through all events.
        for event in pygame.event.get():
            # Terminates program if necessary.
            # Leaves method if either l-shift or r-shift is pressed.
            if event.type == QUIT:
                io.terminate(info)
            elif event.type == KEYUP:
                if event.key == K_ESCAPE:
                    io.terminate(info)
                elif event.key == K_LSHIFT or event.key == K_RSHIFT:
                    pygame.event.get() # Clear queued events.
                    return
''' Program: Tetrate 1.0
    Author: Eric Y. Chang
    Date: 8/9/2019
    Notes:
    - Audio source: https://www.youtube.com/watch?v=rDCOcqpWTkY
    - Everything else created by Author.
'''

import pygame
import random
import sys
from pygame.locals import *
from components import *

def main():
    pygame.init()
    
    # Create pygame-reliant variables and set the caption on the window.
    fpsclock = pygame.time.Clock()
    screen = pygame.display.set_mode((data.screen_width, data.screen_height))
    title_font = pygame.font.SysFont('OCR A Extended', 50)
    basic_font = pygame.font.SysFont('OCR A Extended', 30)
    small_font = pygame.font.SysFont('OCR A Extended', 18)
    pygame.display.set_caption(data.title)
    
    # Loads music and plays it on repeat.
    pygame.mixer.music.load("components/music_A.mp3")
    pygame.mixer.music.play(-1, 0.0)
    
    # Create stats object.
    info = stats.Stats(screen, fpsclock, title_font, basic_font, small_font)
    
    # Show a start screen and control-describing screen.
    graphics.start_screen(info)
    graphics.controls_screen(info)
    
    # Loops until the program is exited.
    while (True):
        # Creates new game.
        info.new_stats()
        new_game = game.Game(info)
        
        # Play the new game until GameOverException is thrown.
        try:
            new_game.play()
        except exceptions.GameOverException:
            # Wait for 1.5 seconds before showing save screen, if the
            # score is a high score.  Regardless, the start screen will
            # again be shown.
            info.fpsclock.tick(0.75)
            if (info.is_high_score(info.score)):
                graphics.save_screen(info)
            graphics.start_screen(info)


if __name__ == '__main__':
    main()